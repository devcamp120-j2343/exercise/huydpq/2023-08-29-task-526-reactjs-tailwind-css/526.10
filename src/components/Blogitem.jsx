const BlogItem = ({data}) => {
    return (
        <div className="flex flex-col mb-[90px]">
            <img src={data.image} alt="a" className="rounded-md"/>
            <p className="w-[115px] mt-[30px] mb-[21px] bg-[#3056D3] text-white px-[17px] py-[4px] rounded text-xs not-italic font-semibold leading-6">{data.time}</p>
            <h2 className="mb-[15px] text-[#212B36] not-italic font-semibold text-2xl">{data.title}</h2>
            <p className="text-[#637381] text-base font-normal not-italic">{data.description}</p>
        </div>
    )
}

export default BlogItem