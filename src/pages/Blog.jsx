import BlogItem from "../components/Blogitem"
import items from "../data"
const Blog = () => {
    return (
        <>
            <div className="text-[#3056D3] text-center text-lg font-semibold not-italic mt-[90px]">Our Blogs</div>
            <div className="text-[#2E2E2E] text-center text-[40px] font-bold not-italic mt-2">Our Recent News</div>
            <div className="text-[#637381] text-center text-[15px] font-normal not-italic mt-[15px] max-w-[770px] mx-auto leading-[25px] mb-[80px]">
                There are many variations of passages of Lorem Ipsum available
                but the majority have suffered alteration in some form.
            </div>
            <div className="grid grid-cols-3 max-w-[1440px] mx-auto gap-[30px]">
                {
                    items.map((item) => {
                        return <BlogItem data={item} />
                    })
                }
            </div>

        </>
    )
}

export default Blog